//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_CONTROLLER_H
#define CCF_CONTROLLER_H

#include <functional>
#include <optional>

#include <ros/ros.h>

#include "ccf/connection/Connection.h"

#include <Scene.pb.h>

class Controller
{
private:
  std::map<std::string, std::vector<std::function<void(const std::string)>>> callbacks_;

protected:
  ros::NodeHandle node_handle_;
  std::vector<std::shared_ptr<Connection>> connections;

  void receive(const std::string& channel, const std::string& data);

public:
  explicit Controller(const ros::NodeHandle& nodeHandle);

  // common functionality
  void addConnection(const std::shared_ptr<Connection>& connection);

  void addCallback(const std::string& channel, const std::function<void(const std::string)>& function);

  void sendToAll(const std::string& channel, const std::string& message);
};

#endif  // CCF_CONTROLLER_H
