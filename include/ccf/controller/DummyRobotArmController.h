//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_DUMMYROBOTARMCONTROLLER_H
#define CCF_DUMMYROBOTARMCONTROLLER_H

#include "RobotArmController.h"

class DummyRobotArmController : public virtual RobotArmController
{
public:
  explicit DummyRobotArmController(const ros::NodeHandle& nodeHandle, const std::string& cellName,
                                   const std::string& robotName);

  bool pickAndDrop(Object& robot, Object& object, Object& bin, bool simulateOnly) override;

  bool pickAndPlace(Object& robot, Object& object, Object& location, bool simulateOnly) override;

  bool moveToPose(Object& robot, geometry_msgs::Pose pose, bool simulateOnly) override;

  bool configureCollaborationZone(Object& zone, const std::string& owner) override;

  /// Compute if a location is reachable by a robot, i.e., if an object can be placed or picked at this location
  /// If the location is within a "cone" with a radius of 150mm around the robot base, it is too close to reach.
  /// If the location is more than 750mm away from the robot base it is too far away.
  /// Otherwise, we assume that we can reach it.
  /// \param robot
  /// \param location
  /// \param object to be picked and placed. The current location of this object is ignored.
  /// \return true if the location is reachable
  bool reachableLocation(const Object& robot, const Object& location, const Object& object) override;

  /// Compute if an object is reachable by a robot, i.e., if an object can grasp it
  /// \param robot
  /// \param object to be picked
  /// \return true if the location is reachable
  bool reachableObject(const Object& robot, const Object& object) override;
  bool pick(const std::string& robot_name, const std::string& object_name, bool simulateOnly) override;
  bool place(const std::string& robot_name, const std::string& location_name, bool simulateOnly) override;
  bool drop(const std::string& robot_name, const std::string& bin_name, bool simulateOnly) override;
};

#endif  // CCF_DUMMYROBOTARMCONTROLLER_H
