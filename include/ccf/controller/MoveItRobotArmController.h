/*! \file MoveItRobotArmController.h

    \author Johannes Mey
    \author Sebastian Ebert
    \date 17/01/2021
*/

#ifndef CCF_MOVEITROBOTARMCONTROLLER_H
#define CCF_MOVEITROBOTARMCONTROLLER_H

#include <mutex>

#include <std_msgs/String.h>

#include <tf2_ros/transform_listener.h>

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

#include <google/protobuf/util/json_util.h>
#include <google/protobuf/message.h>

#include "RobotArmController.h"

class MoveItRobotArmController : public virtual RobotArmController
{
private:
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface_;
  moveit::planning_interface::MoveGroupInterface group_;

  //  std::string current_picked_object_id_;  // TODO move this to the RobotArmController and use it in the lambdas

  std::mutex planning_scene_update_in_progress_;

  /// position and orientation of the robot used to translate to moveit coordinates
  /// TODO use pose instead of position
  geometry_msgs::Pose current_transform_;

  // TODO get rid of magic number
  const double OPEN_AMOUNT = 0.078;

  bool is_simulated_robot_ = false;

  /**
   * tf2 buffer to receive current robot state
   */
  tf2_ros::Buffer tf_buffer_;

  /**
   * Constructs an scene-update-object and updates it in the CCF controller.
   * @return true if the scene has changed
   */
  bool pushSceneUpdate();

  /**
   * An object is physical if it may appear in the MoveitPlanningScene.
   * This is true for standard objects (that are STATIONARY, i.e., not currently being moved), bins, boxes, and
   * collaboration zones. Note that zones are physical, but might still be disabled!
   * @param object a reference to the object
   * @return true if the object is physical
   */
  static bool isPhysical(const Object& object)
  {
    return object.type() == Object_Type_UNKNOWN || object.type() == Object_Type_BIN ||
           (object.type() == Object_Type_BOX && object.state() == Object_State_STATE_STATIONARY) ||
           object.type() == Object_Type_COLLABORATION_ZONE;
  }

  /** transform the pose from the scene to the moveit world
   * FIXME it does not consider the orientation, which is assumed to be identical in both worlds
   * @param scenePose
   * @return
   */
  geometry_msgs::Pose fromSceneToMoveIt(const geometry_msgs::Pose& scenePose) const;

  /**
   * Get the support surface for an object
   * @param object_to_pick the object of interest
   * @return the id of the support surface or an empty string if non could be found
   */
  std::string getSupportSurface(const moveit_msgs::CollisionObject& object_to_pick);

  /**
   * Get the support surface for an object when placed at a pose
   * @param target_object the id object of interest
   * @param pose the pose the object is considered to be at
   * @return the id of the support surface or an empty string if non could be found
   */
  std::string getSupportSurface(const std::string& target_object, const geometry_msgs::Pose& pose);

  /**
   * Transform scene object to MoveIt collision object
   * @param part the scene object
   * @param robotPosition the robot position used to translate the coordinates to the MoveIt coordinate system
   * @return a new collision object
   */
  static moveit_msgs::CollisionObject
  toCollisionObject(const Object& part, const geometry_msgs::Point& robotPosition,
                    moveit_msgs::CollisionObject::_operation_type operation = moveit_msgs::CollisionObject::ADD);

  /**
   * Transform scene object into the color of a MoveIt object
   * @param part the scene object
   * @return a new color object
   */
  static moveit_msgs::ObjectColor toCollisionObjectColor(const Object& part);

public:
  explicit MoveItRobotArmController(ros::NodeHandle& nodeHandle, const std::string& cellName,
                                    const std::string& robotName);

  virtual ~MoveItRobotArmController();

  bool pickAndDrop(Object& robot, Object& object, Object& bin, bool simulate_only) override;

  bool pickAndPlace(Object& robot, Object& object, Object& location, bool simulate_only) override;

  bool moveToPose(Object& robot, geometry_msgs::Pose pose, bool simulateOnly) override;

  bool configureCollaborationZone(Object& zone, const std::string& owner) override;

  bool reachableLocation(const Object& robot, const Object& location, const Object& object) override;

  bool reachableObject(const Object& robot, const Object& object) override;

  void initScene(const Scene& scene, bool sendUpdates) override;
  bool pick(const std::string& robot_id, const std::string& object_id, bool simulate_only) override;
  bool drop(const std::string& robot_id, const std::string& bin_id, bool simulate_only) override;
  bool place(const std::string& robot_id, const std::string& location_name, bool simulate_only) override;
  std::string attachedBox();

  /**
   * Specify the workspace bounding described by the two points
   * @param minPosition
   * @param maxPosition
   * @param useRobotCoordinateSystem if true, the points are relative to the robot, not the scene
   */
  void setWorkspace(geometry_msgs::Point minPosition, geometry_msgs::Point maxPosition, bool useRobotCoordinateSystem);

private:
  bool doPick(const std::string& robot_id, const std::string& object_id, bool simulate_only, bool setIdleAfterwards);
};

#endif  // CCF_MOVEITROBOTARMCONTROLLER_H
