//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_ROBOTARMCONTROLLER_H
#define CCF_ROBOTARMCONTROLLER_H

#include <functional>
#include <optional>

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

#include "ccf/connection/Connection.h"
#include "Controller.h"

#include <Scene.pb.h>

class RobotArmController : public virtual Controller
{
private:
  std::function<void(Selection)> selection_action_;
  std::function<void()> scene_update_action_;
  std::function<void(Command)> command_action_;

  std::string cell_name_;

  std::string selection_topic_;
  std::string init_scene_topic_;
  std::string send_scene_topic_;
  std::string send_delta_scene_topic_;
  std::string command_topic_;

protected:
  std::vector<std::unique_ptr<Connection>> connections_;
  std::shared_ptr<Scene> scene_;
  std::string robot_name_;

  bool removeObject(const Object& object);

public:
  const std::string& getSelectionTopic() const;

  void setSelectionTopic(const std::string& selectionTopic);

  const std::string& getInitSceneTopic() const;

  void setInitSceneTopic(const std::string& initSceneTopic);

  const std::string& getSendSceneTopic() const;

  void setSendSceneTopic(const std::string& sendSceneTopic);

  const std::string& getSendDeltaSceneTopic() const;

  void setSendDeltaSceneTopic(const std::string& sendSceneTopic);

  const std::string& getCommandTopic() const;

  void setCommandTopic(const std::string& commandTopic);

  virtual bool configureCollaborationZone(Object& zone, const std::string& owner);

  const std::string& getRobotName() const;
  void setRobotName(const std::string& robot_name);

  RobotArmController(const ros::NodeHandle& nodeHandle, const std::string& cellName, const std::string& robotName);

  bool loadScene(const std::string& sceneFile);

  std::shared_ptr<Scene> getScene();

  void sendScene();

  void sendScene(const std::vector<std::string>& modified, const std::vector<std::string>& added,
                 const std::vector<std::string>& removed);

  virtual void initScene(const Scene& scene, bool sendUpdates = true);

  /// Pick an object
  /// \param robot_name the robot to pick the object. If it is not controlled by this controller, the task is ignored.
  /// \param object_name the name of the object to be picked
  /// \param simulateOnly perform a dry-run only, do not actually execute the task
  /// \return true if picking was successful
  virtual bool pick(const std::string& robot_name, const std::string& object_name, bool simulateOnly) = 0;

  /// Place an object onto a location
  /// \param robot_name the robot to place the object. If it is not controlled by this controller, the task is ignored.
  /// \param location_name the name of the location
  /// \param simulateOnly perform a dry-run only, do not actually execute the task
  /// \return true if placing was successful
  virtual bool place(const std::string& robot_name, const std::string& location_name, bool simulateOnly) = 0;

  /// Drop an object into a container
  /// \param robot_name the robot to drop the object. If it is not controlled by this controller, the task is ignored.
  /// \param bin_name the name of the container
  /// \param simulateOnly perform a dry-run only, do not actually execute the task
  /// \return true if the dropping was successful
  virtual bool drop(const std::string& robot_name, const std::string& bin_name, bool simulateOnly) = 0;

  /// A combination of the pick and the drop operations
  /// \param robot
  /// \param object
  /// \param bin
  /// \param simulateOnly
  /// \return
  virtual bool pickAndDrop(Object& robot, Object& object, Object& bin, bool simulateOnly);

  virtual bool pickAndPlace(Object& robot, Object& object, Object& location, bool simulateOnly);

  /// Evacuate a certain region
  /// \param robot
  /// \param space
  /// \param simulateOnly
  /// \return
  virtual bool moveToPose(Object& robot, geometry_msgs::Pose pose, bool simulateOnly);

  /// Compute if a location is reachable by a robot, i.e., if an object can be placed or picked at this location
  /// \param robot
  /// \param location
  /// \param object to be picked and placed. The current location of this object is ignored.
  /// \return true if the location is reachable
  virtual bool reachableLocation(const Object& robot, const Object& location, const Object& object) = 0;

  /// Compute if an object is reachable by a robot, i.e., if an object can grasp it
  /// \param robot
  /// \param object to be picked
  /// \return true if the location is reachable
  virtual bool reachableObject(const Object& robot, const Object& object) = 0;

  /// Setter for a callback that is called when a selection message is sent to the scene
  /// \param callback function that is given the received selection message
  void reactToSelectionMessage(std::function<void(Selection)> callback);

  /// Setter for a callback that is called when a command message is sent to the scene
  /// \param callback function that is given the received command message
  void reactToCommandMessage(std::function<void(Command)> callback);

  /// Setter for a callback that is called when a the scene is updated
  /// \param callback function that is given the updated scene
  void reactToSceneUpdateMessage(std::function<void()> callback);

  /**
   * resolve an object by name in the current scene
   * @param the name of the object
   * @return the object in the current scene
   */
  Object* resolveObject(const std::string& id);

  /**
   * resolve an object by name in a given scene
   * @param the name of the object
   * @param scene a scene
   * @return the object in the current scene
   */
  static Object* resolveObject(const std::string& id, Scene& scene);

  /**
   * resolve an object by name in a given scene
   * @param the name of the object
   * @param scene a scene
   * @return a const reference to the object in the provided scene
   */
  static std::optional<const Object> resolveObject(const std::string& id, const Scene& scene);

  static Scene loadSceneFromFile(const std::string& sceneFile);

  /**
   * Sends a delta update on the delta update channel
   * @param added Objects in the current scene that have been newly added
   * @param removed Objects no longer in the current scene
   * @param changed Objects that have been modified in the current scene
   */
  void sendDelta(const std::vector<std::string>& added, const std::vector<std::string>& removed,
                 const std::vector<std::string>& changed);
};

#endif  // CCF_ROBOTARMCONTROLLER_H
