//
// Created by Sebastian Ebert on 04.05.21.
//

#ifndef CCF_MQTTCONNECTION_H
#define CCF_MQTTCONNECTION_H

#include "Connection.h"

#include <mqtt/client.h>

class MqttConnection : virtual public Connection
{
  std::vector<std::string> topics;
  const int QOS = 0;
  mqtt::async_client cli_;
  mqtt::connect_options connOpts_;

public:
  MqttConnection(const std::string& mqttAddress, const std::string& client_id);

  bool send(const std::string& channel, const std::string& message) override;

  bool initializeConnection(std::function<void(std::string, std::string)> callback) override;

  bool listen(const std::string& topic) override;

  ~MqttConnection();
};

#endif  // CCF_MQTTCONNECTION_H
