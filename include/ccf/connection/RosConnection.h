//
// Created by Sebastian Ebert on 29.10.21.
//
#ifndef SRC_ROSCONNECTION_H
#define SRC_ROSCONNECTION_H

#include "Connection.h"
#include <thread>
#include <vector>
#include <ros/ros.h>
#include <map>

class RosConnection : public Connection
{
  std::vector<std::string> ros_topics;
  std::unique_ptr<std::thread> ros_spinner_thread;
  std::vector<ros::Subscriber> ros_subscribers;
  std::map<std::string, ros::Publisher> ros_publishers;
  ros::NodeHandle node_handle;
  int input_queue_size;
  int output_queue_size;

public:
  /**
   * A connection to the ROS based communication, limited to std_messages/String.
   * @param n a valid node handle
   * @param input_queue_size of the subscribers
   * @param output_queue_size of the publishers
   */
  explicit RosConnection(const ros::NodeHandle& n, int input_queue_size, int output_queue_size);

  bool send(const std::string& channel, const std::string& message) override;

  bool initializeConnection(std::function<void(std::string, std::string)> callback) override;

  bool listen(const std::string& channel) override;

  /**
   * Grands the possibility to change the node handle and thus the sender namespace.
   * @param n the node handel to be set
   */
  void setHandle(const ros::NodeHandle& n);

  /**
   * Adds a publisher, to be used to publish messages to a certain topic.
   * Needs to be done because there is delay between registering a publisher, and the time where one can use the
   * publisher.
   * @param channel the topic on which the publisher sends
   * @return true if successfull added
   */
  bool addPublisher(const std::string& channel);

  /**
   * Receive messages on all configured topics.
   */
  void receiveMessages();

  ~RosConnection();
};

#endif  // SRC_ROSCONNECTION_H
