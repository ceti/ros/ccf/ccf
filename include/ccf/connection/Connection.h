//
// Created by Johannes Mey on 28/04/2021.
//

#ifndef CCF_CONNECTION_H
#define CCF_CONNECTION_H

#include <string>
#include <functional>

/// This calls represents a connection that can publish and subscribe to channels
class Connection
{
protected:
  /// callback defined by the initializeConnection() method
  std::function<void(std::string, std::string)> messageReceivedCallback;

public:
  /// Listen on a specific channel. By default, this method returns true. It must not be implemented, but then it is
  /// assumed that the connection listen to all channels
  /// \param channel the connection should be listening on
  /// \return true if the connection is able to listen to the channel
  virtual bool listen(const std::string& channel)
  {
    return true;
  };

  /// Initializes the connection by specifying a callback for received messages. This method must be called exactly
  /// once. \param callback to be called when a message is received \return true if the initialization was successful
  virtual bool initializeConnection(std::function<void(const std::string, const std::string)> callback) = 0;

  /// Send a message over a channel
  /// \param channel
  /// \param message
  /// \return true if the message was sent successfully
  virtual bool send(const std::string& channel, const std::string& message) = 0;
};

#endif  // CCF_CONNECTION_H
