#include <ros/ros.h>

#include <string>
#include <map>
#include <vector>

#include "ccf/util/NodeUtil.h"

#include <gtest/gtest.h>

using CetiRosToolbox::getParameter;

class NodeUtilTest : public ::testing::Test
{
protected:
  ros::NodeHandle n;

  void SetUp() override
  {
    /**
     * Register a "node_util_test" node
     */
    n = ros::NodeHandle("node_util_test");

    n.setParam("stringKey", std::string("stringValue"));
    n.setParam("intKey", 42);
    n.setParam("doubleKey", 0.42);
    n.setParam("boolKey", true);
    n.setParam("xmlKey", XmlRpc::XmlRpcValue(42));
    n.setParam("stringListKey", std::vector<std::string>{ "a", "b", "c" });
    n.setParam("boolListKey", std::vector<bool>{ true, false, true });
    n.setParam("intListKey", std::vector<int>{ 1, 2, 4 });
    n.setParam("floatListKey", std::vector<float>{ 0.0f, 1.0f, 0.42f });
    n.setParam("doubleListKey", std::vector<double>{ 0.5, -2.0 });
    std::map<std::string, std::string> stringMap;
    stringMap["key1"] = "value1";
    n.setParam("stringMapKey", stringMap);
    std::map<std::string, int> intMap;
    intMap["key1"] = 42;
    n.setParam("intMapKey", intMap);
  }
};

TEST_F(NodeUtilTest, ReadNonExistingString)
{
  std::string param = "initialValue";
  EXPECT_FALSE(n.getParam("key", param));
  EXPECT_EQ(param, "initialValue");
  auto result = getParameter<std::string>(n, "key");
  ASSERT_EQ(result, "");
}

TEST_F(NodeUtilTest, ReadNonExistingStringWithFallback)
{
  std::string param = "initialValue";
  EXPECT_FALSE(n.getParam("key", param));
  EXPECT_EQ(param, "initialValue");
  auto result = getParameter<std::string>(n, "key", "fallBackParam");
  ASSERT_EQ(result, "fallBackParam");
}

TEST_F(NodeUtilTest, ReadExistingSimpleDataTypes)
{
  {
    auto stringValue = getParameter<std::string>(n, "stringKey");
    ASSERT_EQ(stringValue, std::string("stringValue"));
  }
  {
    auto intValue = getParameter<int>(n, "intKey");
    ASSERT_EQ(intValue, 42);
  }
  {
    auto doubleValue = getParameter<double>(n, "doubleKey");
    ASSERT_EQ(doubleValue, 0.42);
  }
  {
    auto boolValue = getParameter<bool>(n, "boolKey");
    ASSERT_EQ(boolValue, true);
  }
  {
    auto xmlValue = getParameter<XmlRpc::XmlRpcValue>(n, "xmlKey");
    // XmlRpc prints the synonymous types 'int' and 'i4' both as 'i4'
    ASSERT_EQ(xmlValue.toXml(), "<value><i4>42</i4></value>");
  }
}

TEST_F(NodeUtilTest, ReadExistingVectorDataTypes)
{
  {
    auto stringListValue = getParameter<std::vector<std::string>>(n, "stringListKey");
    std::vector<std::string> expectedStringListValue{ "a", "b", "c" };
    ASSERT_EQ(stringListValue, expectedStringListValue);
  }
  {
    auto boolListValue = getParameter<std::vector<bool>>(n, "boolListKey");
    std::vector<bool> expectedBoolListValue{ true, false, true };
    ASSERT_EQ(boolListValue, expectedBoolListValue);
  }
  {
    auto intListValue = getParameter<std::vector<int>>(n, "intListKey");
    std::vector<int> expectedIntListValue{ 1, 2, 4 };
    ASSERT_EQ(intListValue, expectedIntListValue);
  }
  {
    auto floatListValue = getParameter<std::vector<float>>(n, "floatListKey");
    std::vector<float> expectedFloatListValue{ 0.0f, 1.0f, 0.42f };
    ASSERT_EQ(floatListValue, expectedFloatListValue);
  }
  {
    auto doubleListValue = getParameter<std::vector<double>>(n, "doubleListKey");
    std::vector<double> expectedDoubleListValue{ 0.5, -2.0 };
    ASSERT_EQ(doubleListValue, expectedDoubleListValue);
  }
}

TEST_F(NodeUtilTest, ReadExistingMapDataTypes)
{
  {
    auto stringMapValue = getParameter<std::map<std::string, std::string>>(n, "stringMapKey");
    ASSERT_EQ(stringMapValue["key1"], "value1");
  }
  {
    auto intMapValue = getParameter<std::map<std::string, int>>(n, "intMapKey");
    ASSERT_EQ(intMapValue["key1"], 42);
  }
}

TEST_F(NodeUtilTest, ReadExistingSimpleDataTypesWithFallback)
{
  {
    auto stringValue = getParameter<std::string>(n, "stringKey", "fallback");
    ASSERT_EQ(stringValue, std::string("stringValue"));
  }
  {
    auto intValue = getParameter<int>(n, "intKey", -1);
    ASSERT_EQ(intValue, 42);
    intValue = getParameter(n, "intKey", -1);
    ASSERT_EQ(intValue, 42);
  }
  {
    auto doubleValue = getParameter<double>(n, "doubleKey", -1.0);
    ASSERT_EQ(doubleValue, 0.42);
    doubleValue = getParameter(n, "doubleKey", -1.0);
    ASSERT_EQ(doubleValue, 0.42);
  }
  {
    auto boolValue = getParameter<bool>(n, "boolKey", false);
    ASSERT_EQ(boolValue, true);
    boolValue = getParameter(n, "boolKey", false);
    ASSERT_EQ(boolValue, true);
  }
  {
    auto xmlValue = getParameter<XmlRpc::XmlRpcValue>(n, "xmlKey", XmlRpc::XmlRpcValue(23));
    ASSERT_EQ(xmlValue.toXml(), "<value><i4>42</i4></value>");
    xmlValue = getParameter(n, "xmlKey", XmlRpc::XmlRpcValue(23));
    ASSERT_EQ(xmlValue.toXml(), "<value><i4>42</i4></value>");
  }
}

TEST_F(NodeUtilTest, ReadExistingVectorDataTypesWithFallback)
{
  {
    std::vector<std::string> expectedStringListValue{ "a", "b", "c" };
    auto stringListValue = getParameter<std::vector<std::string>>(n, "stringListKey", std::vector<std::string>{ "x" });
    ASSERT_EQ(stringListValue, expectedStringListValue);
    stringListValue = getParameter(n, "stringListKey", std::vector<std::string>{ "x" });
    ASSERT_EQ(stringListValue, expectedStringListValue);
  }
  {
    std::vector<bool> expectedBoolListValue{ true, false, true };
    auto boolListValue = getParameter<std::vector<bool>>(n, "boolListKey", std::vector<bool>{ true });
    ASSERT_EQ(boolListValue, expectedBoolListValue);
    boolListValue = getParameter(n, "boolListKey", std::vector<bool>{ true });
    ASSERT_EQ(boolListValue, expectedBoolListValue);
  }
  {
    auto intListValue = getParameter<std::vector<int>>(n, "intListKey", std::vector<int>{ 7 });
    std::vector<int> expectedIntListValue{ 1, 2, 4 };
    ASSERT_EQ(intListValue, expectedIntListValue);
  }
  {
    auto floatListValue = getParameter<std::vector<float>>(n, "floatListKey", std::vector<float>{ 0.5f });
    std::vector<float> expectedFloatListValue{ 0.0f, 1.0f, 0.42f };
    ASSERT_EQ(floatListValue, expectedFloatListValue);
  }
  {
    auto doubleListValue = getParameter<std::vector<double>>(n, "doubleListKey", std::vector<double>{ 0.5 });
    std::vector<double> expectedDoubleListValue{ 0.5, -2.0 };
    ASSERT_EQ(doubleListValue, expectedDoubleListValue);
  }
}

TEST_F(NodeUtilTest, ReadNonExistingMapDataTypesWithFallback)
{
  {
    auto stringMapValue =
        getParameter<std::map<std::string, std::string>>(n, "unknownKey", std::map<std::string, std::string>{});
    ASSERT_EQ(stringMapValue.size(), 0);
  }
  {
    auto intMapValue = getParameter<std::map<std::string, int>>(n, "unknownKey", std::map<std::string, int>{});
    ASSERT_EQ(intMapValue["key1"], 0);
  }
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "node_util_test");
  return RUN_ALL_TESTS();
}