/*! \file MoveItRobotArmController.cpp

    \author Johannes Mey
    \author Sebastian Ebert
    \date 17/01/2021
*/

#include <ros/ros.h>
#include <limits>

#include <grasp_util.h>
#include <franka_gripper_util.h>
#include <PandaUtil.h>

#include "ccf/controller/MoveItRobotArmController.h"

bool MoveItRobotArmController::pickAndDrop(Object& robot, Object& object, Object& bin, bool simulate_only)
{
  auto bin_id = bin.id();

  auto robot_id = robot.id();
  auto object_id = object.id();

  if (!resolveObject(robot_id))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking aborted, because robot with id " << robot_id
                                                                                          << " in not in the scene.");
    return false;
  }
  if (!resolveObject(object_id))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking aborted, because object with id " << object_id
                                                                                           << " in not in the scene.");
    return false;
  }
  if (!resolveObject(bin_id))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking aborted, because bin with id " << bin_id
                                                                                        << " in not in the scene.");
    return false;
  }
  else if (resolveObject(bin_id)->type() != Object_Type_BIN)
  {
    ROS_ERROR("[MoveItRobotArmController] Tried to drop an object into something that doesn't exist or is no bin.");
  }
  if (simulate_only)
  {
    // TODO implement simulateOnly = true
    ROS_ERROR_STREAM("[MoveItRobotArmController] pick with simulateOnly=true not implemented yet!");
    return false;
  }

  if (!doPick(robot_id, object_id, simulate_only, false))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking failed.");
    // TODO update the state in the scene
    return false;
  }

  // ensure prerequisites for placing
  if (attachedBox().empty())
  {
    ROS_ERROR("[MoveItRobotArmController] Place planning failed: No object currently grasped.");
    return false;
  }

  // the robot is placing, the object is still picked and owned by the robot
  resolveObject(robot_id)->set_state(Object_State_STATE_PLACING);
  sendDelta({}, {}, { robot_id });
  sendScene();

  system("rosrun dynamic_reconfigure dynparam set /move_group/pick_place cartesian_motion_step_size 0.01");

  ROS_INFO_STREAM("[MoveItRobotArmController] Dropping object.");

  return drop(robot_id, bin_id, simulate_only);
}
bool MoveItRobotArmController::drop(const std::string& robot_id, const std::string& bin_id, bool simulate_only)
{
  // TODO implement simulateOnly = true
  if (simulate_only)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] drop with simulateOnly=true not implemented yet!");
    return false;
  }

  // find the currently picked object that is also a scene object
  std::string current_picked_object_id = attachedBox();

  if (current_picked_object_id.empty())
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] unable to drop because no object is attached.");
    return false;
  }

  auto place_pose = planning_scene_interface_.getObjects()[bin_id].pose;
  place_pose.position.z += planning_scene_interface_.getObjects()[bin_id].primitives[0].dimensions[2] / 2;
  auto picked_collision_object = planning_scene_interface_.getAttachedObjects()[current_picked_object_id].object;
  place_pose.position.z += picked_collision_object.primitives[0].dimensions[2] / 2;

  bool placing_was_successful;

  group_.allowReplanning(true);

  ROS_INFO_STREAM("[MoveItRobotArmController] placing object " << picked_collision_object.id << "on support surface "
                                                               << bin_id);
  if (!GraspUtil().placeFromAbove(group_, place_pose, OPEN_AMOUNT, bin_id, current_picked_object_id))
  {
    ROS_INFO("[MoveItRobotArmController] grasp_util.placeFromAbove failed.");
    placing_was_successful = false;
  }
  else
  {
    ROS_INFO("[MoveItRobotArmController] Removing placed object from scene.");
    std::vector<std::string> object_remove_vector{ current_picked_object_id };
    //   planning_scene_interface.removeCollisionObjects(object_remove_vector);

    moveit_msgs::CollisionObject del_obj =
        planning_scene_interface_.getObjects(object_remove_vector).at(current_picked_object_id);
    del_obj.operation = del_obj.REMOVE;
    std::vector<moveit_msgs::CollisionObject> del_obj_vec{ del_obj };
    planning_scene_interface_.applyCollisionObjects(del_obj_vec);

    // this does not look nice
    for (auto it = getScene()->mutable_objects()->begin(); it != getScene()->mutable_objects()->end(); it++)
    {
      if (it->id() == current_picked_object_id)
      {
        getScene()->mutable_objects()->erase(it);
        break;
      }
    }
    pushSceneUpdate();
    placing_was_successful = true;

    // the robot is idle, the object is gone
    resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
    sendDelta({}, { current_picked_object_id }, { robot_id });
    sendScene();
  }
  return placing_was_successful;
}
std::string MoveItRobotArmController::attachedBox()
{
  std::vector<std::string> scene_object_vector;
  for (auto& object : getScene()->objects())
  {
    if (object.type() == Object_Type_BOX)
    {
      scene_object_vector.emplace_back(object.id());
    }
  }

  auto all_attached_scene_objects = planning_scene_interface_.getAttachedObjects(scene_object_vector);

  if (all_attached_scene_objects.size() > 1)
  {
    ROS_ERROR_STREAM("There are more than one object attached to the robot, why?");
    return "";
  }
  else if (all_attached_scene_objects.empty())
  {
    ROS_DEBUG_STREAM("There is no object attached to the robot");
    return "";
  }
  else
  {
    return all_attached_scene_objects.begin()->first;
  }
}

bool MoveItRobotArmController::pickAndPlace(Object& robot, Object& object, Object& location, bool simulate_only)
{
  auto robot_id = robot.id();
  auto object_id = object.id();
  auto location_id = location.id();

  if (!resolveObject(robot_id))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking aborted, because robot with id " << robot_id
                                                                                          << " in not in the scene.");
    return false;
  }
  if (!resolveObject(object_id))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking aborted, because object with id " << object_id
                                                                                           << " in not in the scene.");
    return false;
  }
  if (simulate_only)
  {
    // TODO implement simulateOnly = true
    ROS_ERROR_STREAM("[MoveItRobotArmController] pick with simulateOnly=true not implemented yet!");
    return false;
  }

  if (!doPick(robot_id, object_id, simulate_only, false))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking failed.");
    // TODO update the state in the scene
    return false;
  }

  // ensure prerequisites for placing
  if (attachedBox().empty())
  {
    ROS_ERROR("[MoveItRobotArmController] Place planning failed: No object currently grasped.");
    return false;
  }

  system("rosrun dynamic_reconfigure dynparam set /move_group/pick_place cartesian_motion_step_size 0.01");

  ROS_INFO_STREAM("[MoveItRobotArmController] Placing object.");

  return place(robot_id, location_id, simulate_only);
}
bool MoveItRobotArmController::place(const std::string& robot_id, const std::string& location_name, bool simulate_only)
{
  if (!resolveObject(location_name))
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Picking aborted, because location with id "
                     << location_name << " in not in the scene.");
    return false;
  }

  // TODO implement simulateOnly = true
  if (simulate_only)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] place with simulateOnly=true not implemented yet!");
    return false;
  }

  std::string current_picked_object_id = attachedBox();
  if (current_picked_object_id.empty())
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] unable to place because no object is attached.");
  }

  // the robot is placing, the object is still picked and owned by the robot
  resolveObject(robot_id)->set_state(Object_State_STATE_PLACING);
  sendDelta({}, {}, { robot_id });
  sendScene();

  // transform pose to moveit world
  // The pose submitted to the place algorithm is the BOTTOM center of the location object. The idea behind that is
  // that stuff is placed INSIDE the location.
  geometry_msgs::Pose pose;
  auto location = resolveObject(location_name);
  pose.orientation.x = location->orientation().x();
  pose.orientation.y = location->orientation().y();
  pose.orientation.z = location->orientation().z();
  pose.orientation.w = location->orientation().w();
  pose.position.x = location->pos().x();
  pose.position.y = location->pos().y();
  pose.position.z = location->pos().z() - location->size().height() / 2;

  auto place_pose = this->fromSceneToMoveIt(pose);

  auto picked_collision_object = this->planning_scene_interface_.getAttachedObjects()[current_picked_object_id].object;
  place_pose.position.z += picked_collision_object.primitives[0].dimensions[2] / 2;
  const auto& support_collision_object_id = this->getSupportSurface(picked_collision_object.id, place_pose);

  bool placing_was_successful = false;

  this->group_.allowReplanning(true);

  ROS_INFO_STREAM("[MoveItRobotArmController] placing object " << picked_collision_object.id << "on support surface "
                                                               << support_collision_object_id);
  if (!GraspUtil().placeFromAbove(this->group_, place_pose, this->OPEN_AMOUNT, support_collision_object_id,
                                  current_picked_object_id))
  {
    ROS_INFO("[MoveItRobotArmController] grasp_util.placeFromAbove failed.");
    // the robot is idle, the object is stationary and has no owner
    this->resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
    if (attachedBox() != current_picked_object_id)
    {
      this->resolveObject(current_picked_object_id)->set_state(Object_State_STATE_STATIONARY);
      this->resolveObject(current_picked_object_id)->set_owner("");
    }
    sendDelta({}, {}, { robot_id, current_picked_object_id });
    this->sendScene();
    placing_was_successful = false;
  }
  else
  {
    this->pushSceneUpdate();
    placing_was_successful = true;
    // the robot is idle, the object is stationary and has no owner
    this->resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
    this->resolveObject(current_picked_object_id)->set_state(Object_State_STATE_STATIONARY);
    this->resolveObject(current_picked_object_id)->set_owner("");
    sendDelta({}, {}, { robot_id, current_picked_object_id });
    this->sendScene();
  }

  return placing_was_successful;
}
bool MoveItRobotArmController::doPick(const std::string& robot_id, const std::string& object_id, bool simulate_only,
                                      bool setIdleAfterwards)
{
  // TODO implement simulateOnly = true
  if (simulate_only)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] pick with simulateOnly=true not implemented yet!");
    return false;
  }

  // the robot is picking, the object is picked and owned by the robot
  resolveObject(robot_id)->set_state(Object_State_STATE_PICKING);
  resolveObject(object_id)->set_state(Object_State_STATE_PICKED);
  resolveObject(object_id)->set_owner(robot_id);
  sendDelta({}, {}, { robot_id, object_id });
  sendScene();

  if (!is_simulated_robot_)
  {
    ROS_INFO_STREAM("[MoveItRobotArmController] Resetting franka gripper for next action.");
    FrankaGripperUtil frankaUtil;
    frankaUtil.resetGripperForNextAction();
  }

  ROS_INFO_STREAM("[MoveItRobotArmController] Picking object.");
  ROS_INFO("[MoveItRobotArmController] Starting pick planning.");
  bool picking_was_successful = false;
  GraspUtil grasp_util;

  geometry_msgs::Vector3 dimensions;
  moveit_msgs::CollisionObject object_to_pick;

  group_.allowReplanning(true);

  // just attach the objects before the initial pick operation
  std::vector<std::string> known_object_ids = planning_scene_interface_.getKnownObjectNames();
  std::map<std::string, moveit_msgs::CollisionObject> known_objects =
      planning_scene_interface_.getObjects(known_object_ids);

  std::map<std::string, moveit_msgs::CollisionObject> o_map = planning_scene_interface_.getObjects();

  for (auto const& x : o_map)
  {
    if (x.first == object_id)
    {
      ROS_INFO("[MoveItRobotArmController] Found object to pick in planning scene");
      object_to_pick = x.second;
    }
  }

  std::string support_surface_id = getSupportSurface(object_to_pick);

  if (support_surface_id.empty())
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] No support surface found for " << object_to_pick.id << ".");
  }
  else if (!attachedBox().empty())
  {
    ROS_WARN("[MoveItRobotArmController] An object is currently grasped.");
  }
  else
  {
    dimensions.x = object_to_pick.primitives[0].dimensions[0];
    dimensions.y = object_to_pick.primitives[0].dimensions[1];
    dimensions.z = object_to_pick.primitives[0].dimensions[2];

    geometry_msgs::Transform gripper_transform =
        tf_buffer_.lookupTransform("panda_hand", "panda_link8", ros::Time(0)).transform;
    geometry_msgs::Pose pick_pose =
        grasp_util.getPickFromAbovePose(object_to_pick.pose, dimensions, gripper_transform.rotation);

    bool success = grasp_util.pickFromAbove(group_, pick_pose, dimensions, OPEN_AMOUNT, support_surface_id, object_id);
    if (success)
    {
      picking_was_successful = true;
    }
    else
    {
      if (attachedBox() != object_id)
      {
        resolveObject(object_id)->set_state(Object_State_STATE_STATIONARY);
        resolveObject(object_id)->set_owner("");
      }

      ROS_WARN("[MoveItRobotArmController] grasp_util.pickFromAbove() failed.");
    }
  }

  // the robot is idle
  if (setIdleAfterwards)
  {
    resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
  }
  sendDelta({}, {}, { robot_id, object_id });
  sendScene();

  if (!picking_was_successful)
  {
    ROS_WARN("[MoveItRobotArmController] Picking failed.");
  }
  return picking_was_successful;
}

bool MoveItRobotArmController::moveToPose(Object& robot, geometry_msgs::Pose pose, bool simulateOnly)
{
  const auto& robot_id = robot.id();

  group_.allowReplanning(true);

  if (simulateOnly)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] moveToPose with simulateOnly=true not implemented yet!");
    return false;
  }

  // the robot is moving
  resolveObject(robot_id)->set_state(Object_State_STATE_MOVING);
  sendDelta({}, {}, { robot_id });
  sendScene();

  geometry_msgs::Pose moveit_pose = fromSceneToMoveIt(pose);

  group_.setPoseTarget(moveit_pose, "panda_hand");
  moveit::core::MoveItErrorCode rv = group_.move();
  if (rv != moveit::core::MoveItErrorCode::SUCCESS)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] moveToPose failed with error " << rv);
    return false;
  }

  // the robot is idle again
  resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
  sendDelta({}, {}, { robot_id });
  sendScene();

  return true;
}

bool MoveItRobotArmController::configureCollaborationZone(Object& zone, const std::string& owner)
{
  if (zone.owner() == owner)
  {
    ROS_WARN_STREAM(
        "[MoveItRobotArmController] configureCollaborationZone was called, but it already had the desired owner "
        << owner);
    return false;
  }

  planning_scene_update_in_progress_.lock();
  ROS_INFO("[MoveItRobotArmController] Acquired lock in configureCollaborationZone()");
  moveit_msgs::CollisionObject mco;
  zone.set_owner(owner);
  if (zone.owner() != robot_name_)
  {
    // the zone is blocked, because we do NOT own it
    zone.set_owner("");
    mco = toCollisionObject(zone, current_transform_.position);
    mco.operation = moveit_msgs::CollisionObject::ADD;
    ROS_INFO_STREAM("[MoveItRobotArmController] Blocking collaboration zone " << zone.id());
  }
  else
  {
    // we own it
    // we assume the object exists!
    mco = planning_scene_interface_.getObjects().at(zone.id());  // this is a copy
    mco.operation = moveit_msgs::CollisionObject::REMOVE;
    ROS_INFO_STREAM("[MoveItRobotArmController] Freeing collaboration zone " << zone.id());
  }

  auto result = planning_scene_interface_.applyCollisionObject(mco, toCollisionObjectColor(zone).color);
  planning_scene_update_in_progress_.unlock();
  ROS_INFO("[MoveItRobotArmController] Released lock in configureCollaborationZone()");
  return result;
}

bool MoveItRobotArmController::reachableLocation(const Object& robot, const Object& location, const Object& object)
{
  // we pretend the location is an object and try to reach it
  return reachableObject(robot, location);
}

bool MoveItRobotArmController::reachableObject(const Object& robot, const Object& object)
{
  double dx = object.pos().x() - robot.pos().x();
  double dy = object.pos().y() - robot.pos().y();
  double dz = object.pos().z() - robot.pos().z();

  // if the location is within a "cone" with a radius of 50mm around the robot base, it is too close to place
  if (std::sqrt(dx * dx + dy * dy) < 0.05)
  {
    return false;
  }

  // if the location is more than 750mm away from the robot base it is too far away
  if (std::sqrt(dx * dx + dy * dy + dz * dz) > 0.8)
  {
    return false;
  }

  // otherwise, we can reach it
  return true;
}

MoveItRobotArmController::MoveItRobotArmController(ros::NodeHandle& nodeHandle, const std::string& cellName,
                                                   const std::string& robotName)
  : Controller(nodeHandle), RobotArmController(nodeHandle, cellName, robotName), group_("panda_arm")
{
  ros::AsyncSpinner spinner(7);
  spinner.start();

  tf2_ros::TransformListener tfListener(tf_buffer_);

  // initial configs
  ROS_INFO_STREAM("[MoveItRobotArmController] Exporting robot base_link pose.");

  ros::param::get("is_simulated_robot", is_simulated_robot_);

  if (!is_simulated_robot_)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] Using setup for real robot.");

    PandaUtil::recoverFromErrors();
    GraspUtil::homeGripper();

    // TODO this segment of code needs to be removed
    if (!ros::param::has("max_grasp_approach_velocity") || !ros::param::has("max_grasp_approach_acceleration") ||
        !ros::param::has("max_grasp_transition_velocity") || !ros::param::has("max_grasp_transition_acceleration"))
    {
      ROS_WARN_STREAM(
          "[MoveItRobotArmController] Velocities and accelerations are not completely configured. -> Fallback to "
          "default value. ");

      ros::param::set("max_grasp_approach_velocity", 0.05);
      ros::param::set("max_grasp_approach_acceleration", 0.05);

      ros::param::set("max_grasp_transition_velocity", 0.05);
      ros::param::set("max_grasp_transition_acceleration", 0.05);
    }

    if (!ros::param::has("tud_grasp_force"))
    {
      ROS_WARN_STREAM("[MoveItRobotArmController] Grasp force is not configured. -> Fallback to default value. ");
      ros::param::set("tud_grasp_force", 9.5);
    }
    else
    {
      double tud_grasp_force = 0;
      ros::param::get("tud_grasp_force", tud_grasp_force);
      ROS_INFO_STREAM("[MoveItRobotArmController] Applied tud_grasp_force " << tud_grasp_force);
    }
  }
  ROS_INFO_STREAM("[MoveItRobotArmController] Initial ccf setup finished.");

  ros::Subscriber sub_pick =
      node_handle_.subscribe<std_msgs::String>("/robot/pick", 1000, [&](auto& msg) { /* TODO */ });
  ros::Subscriber sub_drop =
      node_handle_.subscribe<std_msgs::String>("/robot/drop", 1000, [&](auto& msg) { /* TODO */ });
  ros::Subscriber sub_scene_push =
      node_handle_.subscribe<std_msgs::Empty>("/manual_scene_push", 1000, [&](auto& msg) { pushSceneUpdate(); });
}

MoveItRobotArmController::~MoveItRobotArmController() = default;

bool MoveItRobotArmController::pushSceneUpdate()
{
  planning_scene_update_in_progress_.lock();
  ROS_INFO("[MoveItRobotArmController] Acquired lock in pushSceneUpdate()");

  bool scene_has_changed = false;
  std::string changedObject;

  Scene newScene(*getScene());

  auto all_objects = planning_scene_interface_.getObjects();
  for (const auto& [name, object] : planning_scene_interface_.getAttachedObjects())
  {
    all_objects[name] = object.object;
  }

  for (const auto& [name, moveit_collision_object] : all_objects)
  {
    auto object = resolveObject(name, newScene);
    if (object == nullptr)
    {
      ROS_WARN_STREAM("Unable to update collision object " << name << " because it is not part of the scene");
      continue;
    }

    auto primitive_pose = moveit_collision_object.pose;

    double old_x = object->pos().x();
    double old_y = object->pos().y();
    double old_z = object->pos().z();

    double new_x = primitive_pose.position.x + current_transform_.position.x;
    double new_y = primitive_pose.position.y + current_transform_.position.y;
    double new_z = primitive_pose.position.z + current_transform_.position.z;

    if (std::fabs(old_x - new_x) + std::fabs(old_y - new_y) + std::fabs(old_z - new_z) >
        10 * std::numeric_limits<float>::epsilon())
    {
      ROS_INFO_STREAM("[MoveItRobotArmController] [Collision Scene Sync] pose of "
                      << name << " changed from " << old_x << "," << old_y << "," << old_z << " to " << new_x << ","
                      << new_y << "," << new_z << ".");
      changedObject = name;
    }

    object->mutable_pos()->set_x(primitive_pose.position.x + current_transform_.position.x);
    object->mutable_pos()->set_y(primitive_pose.position.y + current_transform_.position.y);
    object->mutable_pos()->set_z(primitive_pose.position.z + current_transform_.position.z);

    object->mutable_orientation()->set_w(primitive_pose.orientation.w);
    object->mutable_orientation()->set_x(primitive_pose.orientation.x);
    object->mutable_orientation()->set_y(primitive_pose.orientation.y);
    object->mutable_orientation()->set_z(primitive_pose.orientation.z);
  }

  ROS_INFO_STREAM("[MoveItRobotArmController] [Collision Scene Sync] Updating scene (sending updates)");
  RobotArmController::initScene(newScene, true);
  planning_scene_update_in_progress_.unlock();
  ROS_INFO("[MoveItRobotArmController] Released lock in pushSceneUpdate()");
  return scene_has_changed;
}

void MoveItRobotArmController::initScene(const Scene& scene, bool sendUpdates)
{
  planning_scene_update_in_progress_.lock();
  ROS_INFO("[MoveItRobotArmController] Acquired lock in initScene()");
  auto lockStart = ros::Time::now();

  if (scene.is_delta())
  {
    RobotArmController::initScene(scene, sendUpdates);
    ROS_INFO_STREAM("[MoveItRobotArmController] Performing delta update in MoveIt");

    auto robot = resolveObject(getRobotName(), scene);

    if (robot && (robot->mode() == Object_Mode_MODE_ADD || robot->mode() == Object_Mode_MODE_MODIFY))
    {
      current_transform_.position.x = robot->pos().x();
      current_transform_.position.y = robot->pos().y();
      current_transform_.position.z = robot->pos().z() - (robot->size().height() / 2);  // "normal to moveit"

      current_transform_.orientation.x = robot->orientation().x();
      current_transform_.orientation.y = robot->orientation().y();
      current_transform_.orientation.z = robot->orientation().z();
      current_transform_.orientation.w = robot->orientation().w();
      ROS_DEBUG_STREAM("[MoveItRobotArmController] Using changed transform from arm " << robot->id());
    }

    // store the objects and their colors in a list to later apply to the scene
    std::vector<moveit_msgs::CollisionObject> moveit_object_list;
    std::vector<moveit_msgs::ObjectColor> moveit_color_list;

    for (const auto& part : scene.objects())
    {
      if (isPhysical(part))  // only consider things that might be in a collision model of MoveIt
      {
        if (planning_scene_interface_.getAttachedObjects().count(part.id())) {
          ROS_WARN_STREAM("[MoveItRobotArmController] Not changing object in MoveIt scene, because it is currently being picked.");
          continue;
        }
        switch (part.mode())
        {
          case Object_Mode_MODE_ADD:
          case Object_Mode_MODE_MODIFY:
            moveit_object_list.emplace_back(
                toCollisionObject(part, current_transform_.position, moveit_msgs::CollisionObject::ADD));
            moveit_color_list.emplace_back(toCollisionObjectColor(part));
            break;
          case Object_Mode_MODE_REMOVE:
            moveit_object_list.emplace_back(
                toCollisionObject(part, current_transform_.position, moveit_msgs::CollisionObject::REMOVE));
            break;
          case Object_Mode_MODE_KEEP:
          case Object_Mode_MODE_UNKNOWN:
          default:
            // do nothing
            break;
        }
      }
    }
    // apply the collision objects in moveit
    if (planning_scene_interface_.applyCollisionObjects(moveit_object_list, moveit_color_list)) {
      ROS_INFO("[MoveItRobotArmController] The MoveIt planning scene has been updated with a delta.");
    } else {
      ROS_WARN("[MoveItRobotArmController] The MoveIt planning could not be updated!");
    }
  }
  else
  {
    // before calling the parent method, we save the old scene to figure out removed objects later
    Scene old_scene;
    if (getScene())
    {
      old_scene = *getScene();
    }

    RobotArmController::initScene(scene, sendUpdates);

    // determine position of arm, it is the centre of the moveit scene
    auto robot = resolveObject(getRobotName(), scene);

    if (robot)
    {
      current_transform_.position.x = robot->pos().x();
      current_transform_.position.y = robot->pos().y();
      current_transform_.position.z = robot->pos().z() - (robot->size().height() / 2);  // "normal to moveit"
      ROS_DEBUG_STREAM("[MoveItRobotArmController] Using transform from arm " << robot->id());
    }
    else
    {
      current_transform_.position.x = current_transform_.position.y = current_transform_.position.z = 0;
      ROS_WARN_STREAM("[MoveItRobotArmController] New scene does not contain an arm. using null-transform for objects");
    }

    // store the objects and their colors in a list to later apply to the scene
    std::vector<moveit_msgs::CollisionObject> moveit_object_list;
    std::vector<moveit_msgs::ObjectColor> moveit_color_list;

    // if there are things in the old scene that are no longer in the new scene, we have to delete them in moveit
    std::vector<std::string> deletedObjects;
    for (const auto& part : old_scene.objects())
    {
      if (resolveObject(part.id()) == nullptr)
      {
        ROS_WARN_STREAM("Deleting removed object from scene " << part.id());
        moveit_object_list.emplace_back(
            toCollisionObject(part, current_transform_.position, moveit_msgs::CollisionObject::REMOVE));
      }
    }

    auto attached_objects = planning_scene_interface_.getAttachedObjects();

    for (const auto& part : getScene()->objects())
    {
      // Step 2: Build a collision object
      if (attached_objects.find(part.id()) != attached_objects.end())
      {
        ROS_WARN_STREAM("[MoveItRobotArmController] skipping object "
                        << " because it is attached in the old scene.");
        continue;
      }

      if (isPhysical(part))  // only consider things that might be in a collision model of MoveIt
      {
        if (part.owner() == robot_name_ && part.type() == Object_Type_BOX)
        {
          ROS_WARN_STREAM("[MoveItRobotArmController] Skipping handling of box "
                          << part.id() << " because it is owned by the current robot and thus currently moved");
        }
        else
        {
          auto operation = moveit_msgs::CollisionObject::ADD;
          if (part.type() == Object_Type_COLLABORATION_ZONE && part.owner() == robot_name_)
          {
            // if we own the collaboration zone, the collision object is removed
            operation = moveit_msgs::CollisionObject::REMOVE;
          }
          moveit_object_list.emplace_back(toCollisionObject(part, current_transform_.position, operation));
          moveit_color_list.emplace_back(toCollisionObjectColor(part));
        }
      }
    }

    // apply the collision objects in moveit
    planning_scene_interface_.applyCollisionObjects(moveit_object_list, moveit_color_list);
    ROS_INFO("[MoveItRobotArmController] The full MoveIt planning scene has been updated.");
  }

  // if the scene is a delta and contains a robot change, or it is not a delta, the robot has to be (re-)attached
  if (!scene.is_delta() || resolveObject(getRobotName(), scene))
  {
    // attaching the support surface to the robot to avoid collision errors
    // the robot is a box of size 0 at the position of the robot (which is 0!) FIXME replace the following lines
    moveit_msgs::CollisionObject dummy_robot;
    dummy_robot.primitive_poses.resize(1);
    dummy_robot.primitives.resize(1);
    dummy_robot.primitives[0].type = shape_msgs::SolidPrimitive::BOX;
    dummy_robot.primitives[0].dimensions.resize(3);
    std::string robot_support_surface = getSupportSurface(dummy_robot);
    if (robot_support_surface.empty())
    {
      ROS_ERROR_STREAM("[MoveItRobotArmController] No support surface found for robot. Planning probably won't work.");
    }
    // the support surface is attached to panda_link0 and may (and will) touch panda_link0 and panda_link1
    group_.attachObject(robot_support_surface, "panda_link0", { "panda_link0", "panda_link1" });
  }

  planning_scene_update_in_progress_.unlock();
  ROS_DEBUG_STREAM("[MoveItRobotArmController] Released lock in initScene() after " << (ros::Time::now() - lockStart).toSec() << "s.");
}
moveit_msgs::CollisionObject
MoveItRobotArmController::toCollisionObject(const Object& part, const geometry_msgs::Point& robotPosition,
                                            moveit_msgs::CollisionObject::_operation_type operation)
{
  moveit_msgs::CollisionObject obj;

  obj.header.frame_id = "panda_link0";
  obj.id = part.id();
  obj.operation = operation;
  if (operation == moveit_msgs::CollisionObject::REMOVE)
  {
    // if the operation is remove, we do not have to care about the over values, the name is enough
    return obj;
  }
  obj.pose.position.x = part.pos().x() - robotPosition.x;
  obj.pose.position.y = part.pos().y() - robotPosition.y;
  obj.pose.position.z = part.pos().z() - robotPosition.z;
  obj.pose.orientation.w = part.orientation().w();
  obj.pose.orientation.x = part.orientation().x();
  obj.pose.orientation.y = part.orientation().y();
  obj.pose.orientation.z = part.orientation().z();
  obj.primitives.resize(1);
  obj.primitives[0].type = obj.primitives[0].BOX;
  obj.primitives[0].dimensions.resize(3);
  obj.primitives[0].dimensions[0] = part.size().length();
  obj.primitives[0].dimensions[1] = part.size().width();
  obj.primitives[0].dimensions[2] = part.size().height();
  obj.primitive_poses.resize(1);
  // the pose of the primitive is the "null" pose, but the quaternion still has to be set
  obj.primitive_poses[0].orientation.z = 1;
  return obj;
}

std::string MoveItRobotArmController::getSupportSurface(const moveit_msgs::CollisionObject& object_to_pick)
{
  std::string support_surface_id;
  double support_surface_candidate_z = std::numeric_limits<double>::lowest();

  auto all_objects = planning_scene_interface_.getObjects();
  for (const auto& [name, object] : planning_scene_interface_.getAttachedObjects())
  {
    all_objects[name] = object.object;
  }

  for (const auto& [name, object] : all_objects)
  {
    // we ignore the object itself, because it cannot be its own support surface
    if (name != object_to_pick.id)
    {
      double max_x = object.pose.position.x + (object.primitives[0].dimensions[0] / 2);
      double max_y = object.pose.position.y + (object.primitives[0].dimensions[1] / 2);
      double min_x = object.pose.position.x - (object.primitives[0].dimensions[0] / 2);
      double min_y = object.pose.position.y - (object.primitives[0].dimensions[1] / 2);

      // set the support surface for the pick task
      if (object_to_pick.pose.position.x > min_x && object_to_pick.pose.position.x < max_x &&
          object_to_pick.pose.position.y > min_y && object_to_pick.pose.position.y < max_y)
      {
        if (object.pose.position.z > support_surface_candidate_z)
        {
          support_surface_id = name;
          support_surface_candidate_z = object.pose.position.z;
        }
      }
    }
  }
  if (support_surface_id.empty())
  {
    ROS_ERROR_STREAM("Found NO support surface for object " << object_to_pick.id);
  }
  return support_surface_id;
}

std::string MoveItRobotArmController::getSupportSurface(const std::string& target_object,
                                                        const geometry_msgs::Pose& pose)
{
  std::string support_surface_id;
  double support_surface_candidate_z = std::numeric_limits<double>::lowest();

  auto all_objects = planning_scene_interface_.getObjects();
  for (const auto& [name, object] : planning_scene_interface_.getAttachedObjects())
  {
    all_objects[name] = object.object;
  }

  for (const auto& [name, object] : all_objects)
  {
    // we ignore the object itself, because it cannot be its own support surface
    if (name != target_object)
    {
      double max_x = object.pose.position.x + (object.primitives[0].dimensions[0] / 2);
      double max_y = object.pose.position.y + (object.primitives[0].dimensions[1] / 2);
      double max_z = object.pose.position.z + (object.primitives[0].dimensions[2] / 2);
      double min_x = object.pose.position.x - (object.primitives[0].dimensions[0] / 2);
      double min_y = object.pose.position.y - (object.primitives[0].dimensions[1] / 2);

      ROS_DEBUG_STREAM("looking at " << name << " minx " << min_x << " maxx " << max_x << " miny " << min_y << " maxy "
                                     << max_y);
      // we assume the object to be ABOVE its support surface
      if (pose.position.z > max_z)
      {
        if (pose.position.x > min_x && pose.position.x < max_x && pose.position.y > min_y && pose.position.y < max_y)
        {
          if (object.pose.position.z > support_surface_candidate_z)
          {
            ROS_DEBUG_STREAM("    best candidate");
            support_surface_id = name;
            support_surface_candidate_z = object.pose.position.z;
          }
          else
          {
            ROS_DEBUG_STREAM("    not best candidate " << object.pose.position.z << " < "
                                                       << support_surface_candidate_z);
          }
        }
        else
        {
          ROS_DEBUG_STREAM("    out of xy range x " << pose.position.x << " y " << pose.position.y);
        }
      }
      else
      {
        ROS_DEBUG_STREAM("    z is too low " << pose.position.z << " < " << max_z);
      }
    }
  }

  if (support_surface_id.empty())
  {
    ROS_ERROR_STREAM("Unable to find support surface for pose at (" << pose.position.x << "," << pose.position.y << ","
                                                                    << pose.position.z << ").");
    return "";
  }
  else
  {
    if (auto support_surface_object = resolveObject(support_surface_id))
    {
      ROS_DEBUG_STREAM("Found support surface for pose at ("
                       << pose.position.x << "," << pose.position.y << "," << pose.position.z << "): "
                       << support_surface_id << " of type " << Object_Type_Name(support_surface_object->type()));
    }
    else
    {
      ROS_DEBUG_STREAM("Found support surface for pose at (" << pose.position.x << "," << pose.position.y << ","
                                                             << pose.position.z << "): " << support_surface_id
                                                             << " not included in the scene");
    }
    return support_surface_id;
  }
}

geometry_msgs::Pose MoveItRobotArmController::fromSceneToMoveIt(const geometry_msgs::Pose& scenePose) const
{
  // current_transform is the position of the robot in the scene, so this must be subtracted
  auto moveItPose = scenePose;
  moveItPose.position.x -= current_transform_.position.x;
  moveItPose.position.y -= current_transform_.position.y;
  moveItPose.position.z -= current_transform_.position.z;
  
  moveItPose.orientation = scenePose.orientation;
  // FIXME impl and test if orientation is correct 
  // (note that geometry_msgs::Quaternions multiply and inverse does not exist, use Eigen::Quaternions or impl yourself)
  // i think it should be ->
  // moveItPose.orientation = scenePose.orientation * inverse(current_transform_.orientation);
  // if it is incorrect it might just not be the inverse, try it ->
  // moveItPose.orientation = scenePose.orientation * current_transform_.orientation;

  return moveItPose;
}
moveit_msgs::ObjectColor MoveItRobotArmController::toCollisionObjectColor(const Object& part)
{
  moveit_msgs::ObjectColor color;
  color.id = part.id();
  color.color.r = part.color().r();
  color.color.g = part.color().g();
  color.color.b = part.color().b();
  color.color.a = part.type() == Object_Type_COLLABORATION_ZONE ? 0.3 : 1;  // TODO support transparent objects
  return color;
}
bool MoveItRobotArmController::pick(const std::string& robot_id, const std::string& object_id, bool simulate_only)
{
  return doPick(robot_id, object_id, simulate_only, true);
}
void MoveItRobotArmController::setWorkspace(geometry_msgs::Point minPosition, geometry_msgs::Point maxPosition, bool useRobotCoordinateSystem)
{
  if (!useRobotCoordinateSystem) {

    geometry_msgs::Pose minPose, maxPose;
    minPose.position = minPosition;
    maxPose.position = maxPosition;
    minPosition = fromSceneToMoveIt(minPose).position;
    maxPosition = fromSceneToMoveIt(maxPose).position;
  }
  group_.setWorkspace(minPosition.x, minPosition.y, minPosition.z, maxPosition.x, maxPosition.y, maxPosition.z);
}
