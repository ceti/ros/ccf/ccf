//
// Created by Johannes Mey on 17/01/2021.
//

#include <fstream>
#include <memory>

#include "ccf/controller/Controller.h"

void Controller::addConnection(const std::shared_ptr<Connection>& connection)
{
  connection->initializeConnection([this](auto&& channel, auto&& data) {
    receive(std::forward<decltype(channel)>(channel), std::forward<decltype(data)>(data));
  });
  connections.emplace_back(connection);
}

Controller::Controller(const ros::NodeHandle& nodeHandle) : node_handle_(nodeHandle)
{
}

void Controller::sendToAll(const std::string& channel, const std::string& message)
{
  for (auto& connection : connections)
  {
    connection->send(channel, message);
  }
}

void Controller::receive(const std::string& channel, const std::string& data)
{
  ROS_DEBUG_STREAM("[Controller] Got message on channel '" << channel << "' of length " << data.length());
  for (const auto& function : callbacks_[channel])
  {
    function(data);
  }
}

void Controller::addCallback(const std::string& channel, const std::function<void(const std::string)>& function)
{
  ROS_INFO_STREAM("[Controller] Adding callback to channel " << channel);
  callbacks_[channel].emplace_back(function);
}
