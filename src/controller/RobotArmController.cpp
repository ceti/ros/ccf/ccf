#include <fstream>
#include <stdexcept>
#include <memory>
#include <utility>
#include <google/protobuf/text_format.h>
#include <google/protobuf/util/json_util.h>

#include "ccf/util/NodeUtil.h"
#include "ccf/controller/RobotArmController.h"

using CetiRosToolbox::getParameter;

Object* RobotArmController::resolveObject(const std::string& id)
{
  for (int i = 0; i < scene_->objects_size(); i++)
  {
    if (scene_->objects(i).id() == id)
    {
      return scene_->mutable_objects(i);
    }
  }
  return nullptr;
}

Object* RobotArmController::resolveObject(const std::string& id, Scene& scene)
{
  for (int i = 0; i < scene.objects_size(); i++)
  {
    if (scene.objects(i).id() == id)
    {
      return scene.mutable_objects(i);
    }
  }
  return nullptr;
}

std::optional<const Object> RobotArmController::resolveObject(const std::string& id, const Scene& scene)
{
  for (int i = 0; i < scene.objects_size(); i++)
  {
    if (scene.objects(i).id() == id)
    {
      return { scene.objects(i) };
    }
  }
  return {};
}

void RobotArmController::sendScene()
{
  if (scene_)
  {  // meaning if the (smart) pointer is not a nullptr

    ROS_INFO_STREAM("[RobotArmController] Sending scene with " << scene_->objects().size() << " objects.");
    sendToAll(send_scene_topic_, scene_->SerializeAsString());
    scene_update_action_();
  }
  else
  {
    ROS_WARN_STREAM("[RobotArmController] Scene is not initialized yet. not sending it.");
  }
}

bool RobotArmController::removeObject(const Object& object)
{
  for (auto it = scene_->mutable_objects()->begin(); it != scene_->mutable_objects()->end(); ++it)
  {
    if (it->id() == object.id())
    {
      ROS_INFO_STREAM("[RobotArmController] Erasing " << it->id() << " from scene.");
      scene_->mutable_objects()->erase(it);
      return true;
    }
  }
  return false;
}

bool RobotArmController::pickAndDrop(Object& robot, Object& object, Object& bin, bool simulateOnly)
{
  return removeObject(object);
}

void RobotArmController::initScene(const Scene& newScene, bool sendUpdates)
{
  if (scene_)
  {  // meaning if the (smart) pointer is not a nullptr
    ROS_INFO_STREAM("[RobotArmController] Resetting scene. New scene has " << newScene.objects().size() << " objects.");
  }
  else
  {
    ROS_INFO_STREAM("[RobotArmController] Initializing scene. New scene has " << newScene.objects().size()
                                                                              << " objects.");
  }
  if (newScene.is_delta())
  {
    if (!scene_)
    {
      // if there is no scene, create an empty one
      ROS_WARN_STREAM("Updating an empty scene with a delta update!");
      scene_ = std::make_unique<Scene>(Scene());
    }

    for (const auto& object : newScene.objects())
    {
      auto oldObject = resolveObject(object.id());
      if (object.mode() == Object_Mode_MODE_MODIFY && oldObject)
      {
        ROS_INFO_STREAM("[RobotArmController] DELTA: Modifying object " << object.id());
        resolveObject(object.id())->CopyFrom(object);
      }
      else if (object.mode() == Object_Mode_MODE_ADD || (object.mode() == Object_Mode_MODE_MODIFY && !oldObject))
      {
        ROS_INFO_STREAM("[RobotArmController] DELTA: Adding object " << object.id());

        if (oldObject) {
          if (oldObject->state() == Object_State_STATE_PICKED) {
            ROS_WARN_STREAM("[RobotArmController] DELTA: Not adding object " << object.id() << " because it is already in scene and currently being picked!");
          } else {
            ROS_WARN_STREAM("[RobotArmController] DELTA: Adding object " << object.id() << ", but it was already in scene with state " << object.state());
            oldObject->CopyFrom(object);
          }
        } else {
          auto newObject = scene_->add_objects();
          *newObject = object;
        }
      }
      else if (object.mode() == Object_Mode_MODE_REMOVE)
      {
        ROS_INFO_STREAM("[RobotArmController] DELTA: Deleting object " << object.id());
        for (auto it = scene_->mutable_objects()->begin(); it != scene_->mutable_objects()->end(); it++)
        {
          if (it->id() == object.id())
          {
            scene_->mutable_objects()->erase(it);
            break;
          }
        }
      }
      else
      {
        // do nothing
      }
    }
    ROS_INFO_STREAM("New scene has " << scene_->objects_size() << " objects after delta update.");
  }
  else
  {
    scene_ = std::make_unique<Scene>(newScene);
  }
  if (sendUpdates)
  {
    sendScene();
  }
}

void RobotArmController::reactToSelectionMessage(std::function<void(Selection)> callback)
{
  selection_action_ = std::move(callback);
}

RobotArmController::RobotArmController(const ros::NodeHandle& nodeHandle, const std::string& cellName,
                                       const std::string& robotName)
  : Controller(nodeHandle)
  , scene_(nullptr)
  , scene_update_action_([]() { ROS_DEBUG_STREAM("Running unset sceneUpdateAction"); })
  , command_action_([](const Command& c) { ROS_DEBUG_STREAM("Running unset pickPlaceAction"); })
  , selection_action_([](const Selection& s) { ROS_DEBUG_STREAM("Running unset selectionPlaceAction"); })
  , cell_name_(cellName)
  , robot_name_(robotName)
{
  setSelectionTopic(getParameter<std::string>(nodeHandle, "topics/selection", "selection"));
  setInitSceneTopic(getParameter(nodeHandle, "topics/initScene", cellName + "/scene/init"));
  setSendSceneTopic(getParameter(nodeHandle, "topics/sendScene", cellName + "/scene/update"));
  setSendDeltaSceneTopic(getParameter(nodeHandle, "topics/sendDeltaScene", cellName + "/scene/delta-update"));
  setCommandTopic(getParameter(nodeHandle, "topics/command", cellName + "/command"));
}

bool RobotArmController::pickAndPlace(Object& robot, Object& object, Object& location, bool simulateOnly)
{
  if (object.orientation().x() < std::numeric_limits<double>::epsilon() &&
      object.orientation().y() < std::numeric_limits<double>::epsilon() &&
      location.orientation().x() < std::numeric_limits<double>::epsilon() &&
      location.orientation().y() < std::numeric_limits<double>::epsilon())
  {
    // the objects must not be rotated around z
    object.mutable_pos()->set_x(location.pos().x());
    object.mutable_pos()->set_y(location.pos().y());
    object.mutable_pos()->set_z(location.pos().z() - location.size().height() / 2 + object.size().height() / 2);
    object.mutable_orientation()->set_z(location.orientation().z());
    object.mutable_orientation()->set_w(location.orientation().w());
    return true;
  }
  else
  {
    return false;
  }
}

void RobotArmController::reactToCommandMessage(std::function<void(Command)> callback)
{
  command_action_ = std::move(callback);
}

void RobotArmController::reactToSceneUpdateMessage(std::function<void()> callback)
{
  scene_update_action_ = std::move(callback);
}

std::shared_ptr<Scene> RobotArmController::getScene()
{
  return scene_;
}

bool RobotArmController::loadScene(const std::string& sceneFile)
{
  Scene newScene = loadSceneFromFile(sceneFile);

  std::string s;
  if (google::protobuf::TextFormat::PrintToString(newScene, &s))
  {
    ROS_INFO_STREAM("[RobotArmController] Parsed a scene with " << newScene.objects().size() << " objects.");
    ROS_DEBUG_STREAM("[RobotArmController] Received scene" << std::endl << s);
    ROS_WARN_STREAM("Updating scene from file");

    for (Object& o : *newScene.mutable_objects())
    {
      if (o.type() == Object_Type_ARM)
      {
        o.set_state(Object_State_STATE_IDLE);
      }
      else if (o.type() == Object_Type_BOX)
      {
        o.set_state(Object_State_STATE_STATIONARY);
      }
    }

    initScene(newScene, true);
    return true;
  }
  else
  {
    ROS_WARN_STREAM("[RobotArmController] Scene invalid! partial content: " << newScene.ShortDebugString());
    return false;
  }
}
Scene RobotArmController::loadSceneFromFile(const std::string& sceneFile)
{  // read file into string: https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
  std::ifstream t(sceneFile);

  if (!t.is_open())
  {
    ROS_ERROR_STREAM("[RobotArmController] Unable to open scene config file " << sceneFile);
    return Scene();
  }

  std::string str;

  t.seekg(0, std::ios::end);  // skip to end of file
  str.reserve(t.tellg());     // reserve memory in the string of the size of the file
  t.seekg(0, std::ios::beg);  // go back to the beginning

  // Note the double parentheses! F**k C++! https://en.wikipedia.org/wiki/Most_vexing_parse
  str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

  Scene newScene;
  google::protobuf::util::Status status = google::protobuf::util::JsonStringToMessage(str, &newScene);
  if (!status.ok())
  {
    ROS_ERROR_STREAM("[RobotArmController] Unable to parse Json String: " << status.ToString());
    return Scene();
  }
  return newScene;
}

const std::string& RobotArmController::getSelectionTopic() const
{
  return selection_topic_;
}

void RobotArmController::setSelectionTopic(const std::string& selectionTopic)
{
  RobotArmController::selection_topic_ = selectionTopic;
  addCallback(selectionTopic, [&](const std::string& data) {
    Selection selection;
    selection.ParseFromString(data);
    selection_action_(selection);
  });
}

const std::string& RobotArmController::getInitSceneTopic() const
{
  return init_scene_topic_;
}

void RobotArmController::setInitSceneTopic(const std::string& initSceneTopic)
{
  RobotArmController::init_scene_topic_ = initSceneTopic;
  addCallback(initSceneTopic, [&](const std::string& data) {
    Scene newScene;
    newScene.ParseFromString(data);
    initScene(newScene);
  });
}

const std::string& RobotArmController::getSendSceneTopic() const
{
  return send_scene_topic_;
}

void RobotArmController::setSendSceneTopic(const std::string& sendSceneTopic)
{
  RobotArmController::send_scene_topic_ = sendSceneTopic;
}

const std::string& RobotArmController::getCommandTopic() const
{
  return command_topic_;
}

void RobotArmController::setCommandTopic(const std::string& commandTopic)
{
  RobotArmController::command_topic_ = commandTopic;

  for (auto& connection : connections_)
  {
    connection->listen(commandTopic);
  }

  addCallback(commandTopic, [this](const std::string& data) {
    ROS_DEBUG_STREAM("[RobotArmController] Received robot command!");
    Command command;
    command.ParseFromString(data);
    command_action_(command);
  });
}
bool RobotArmController::moveToPose(Object& robot, geometry_msgs::Pose pose, bool simulateOnly)
{
  ROS_WARN_STREAM("[RobotArmController] Performing No-Op moveToPose.");
  return true;
}
const std::string& RobotArmController::getRobotName() const
{
  return robot_name_;
}
void RobotArmController::setRobotName(const std::string& robot_name)
{
  robot_name_ = robot_name;
}

bool RobotArmController::configureCollaborationZone(Object& zone, const std::string& owner)
{
  ROS_WARN_STREAM("[RobotArmController] Performing No-Op configureCollaborationZone.");
  return true;
}
void RobotArmController::sendScene(const std::vector<std::string>& modified, const std::vector<std::string>& added,
                                   const std::vector<std::string>& removed)
{
  if (scene_)
  {  // meaning if the (smart) pointer is not a nullptr
    Scene delta;
    delta.set_is_delta(true);
    for (const Object& o : getScene()->objects())
    {
      if (std::find(modified.begin(), modified.end(), o.id()) != modified.end())
      {
        auto newObject = delta.add_objects();
        *newObject = *resolveObject(o.id());
        newObject->set_mode(Object_Mode_MODE_MODIFY);
      }
      else if (std::find(added.begin(), added.end(), o.id()) != added.end())
      {
        auto newObject = delta.add_objects();
        *newObject = *resolveObject(o.id());
        newObject->set_mode(Object_Mode_MODE_ADD);
      }
      else if (std::find(removed.begin(), removed.end(), o.id()) != removed.end())
      {
        auto newObject = delta.add_objects();
        *newObject = *resolveObject(o.id());
        newObject->set_mode(Object_Mode_MODE_REMOVE);
      }
      ROS_INFO_STREAM("[RobotArmController] Sending delta scene with " << delta.objects().size() << " objects.");
      sendToAll(send_delta_scene_topic_, delta.SerializeAsString());

      // TODO this needs to be discussed
      if (getSendSceneTopic() != getSendDeltaSceneTopic())
      {
        // send the full scene
        ROS_INFO_STREAM("[RobotArmController] Sending scene with " << scene_->objects().size() << " objects.");
        sendToAll(send_scene_topic_, scene_->SerializeAsString());
        scene_update_action_();
      }
    }
  }
  else
  {
    ROS_WARN_STREAM("[RobotArmController] Scene is not initialized yet. not sending it.");
  }
}
const std::string& RobotArmController::getSendDeltaSceneTopic() const
{
  return send_delta_scene_topic_;
}
void RobotArmController::setSendDeltaSceneTopic(const std::string& sendSceneTopic)
{
  send_delta_scene_topic_ = sendSceneTopic;
}
void RobotArmController::sendDelta(const std::vector<std::string>& added, const std::vector<std::string>& removed,
                                   const std::vector<std::string>& changed)
{
  Scene delta;
  delta.set_is_delta(true);
  for (const auto& a : added)
  {
    auto resolved_a = resolveObject(a);
    if (resolved_a == nullptr)
    {
      ROS_WARN_STREAM("[RobotArmController] unable to add unknown object " << a << " to delta scene as ADD.");
      continue;
    }
    auto a_object = delta.mutable_objects()->Add();
    a_object->CopyFrom(*resolved_a);
    a_object->set_mode(Object_Mode_MODE_ADD);
  }
  for (const auto& r : removed)
  {
    auto resolved_r = resolveObject(r);
    if (resolved_r != nullptr)
    {
      ROS_WARN_STREAM("[RobotArmController] not adding existing object " << r << " to delta scene as REMOVE.");
      continue;
    }
    auto r_object = delta.mutable_objects()->Add();
    r_object->set_id(r);
    r_object->set_mode(Object_Mode_MODE_REMOVE);
  }
  for (const auto& c : changed)
  {
    auto resolved_c = resolveObject(c);
    if (resolved_c == nullptr)
    {
      ROS_WARN_STREAM("[RobotArmController] unable to add unknown object " << c << " to delta scene as MODIFY.");
      continue;
    }
    auto c_object = delta.mutable_objects()->Add();
    c_object->CopyFrom(*resolveObject(c));
    c_object->set_mode(Object_Mode_MODE_MODIFY);
  }
  sendToAll(send_delta_scene_topic_, delta.SerializeAsString());
}