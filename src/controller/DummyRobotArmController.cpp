/*! \file DummyRobotArmController.cpp
    \author Johannes Mey
    \date 17/01/2021
*/

#include <ros/ros.h>
#include <cmath>

#include "ccf/controller/DummyRobotArmController.h"
#include "ccf/controller/Controller.h"

bool DummyRobotArmController::pickAndDrop(Object& robot, Object& object, Object& bin, bool simulateOnly)
{
  ROS_INFO_STREAM("[DummyRobotArmController] \"Picking and dropping\" for 3 seconds...");

  // TODO implement simulateOnly = true
  if (simulateOnly)
  {
    ROS_ERROR_STREAM("[MoveItRobotArmController] pickAndDrop with simulateOnly=true not implemented yet!");
    return false;
  }

  // the robot is picking, the object is picked and owned by the robot
  robot.set_state(Object_State_STATE_PICKING);
  object.set_state(Object_State_STATE_PICKED);
  object.set_owner(robot.id());
  sendScene();

  // "pick" for 1.5 sec
  ros::Rate(ros::Duration(1.5)).sleep();

  // the robot is placing, the object is still picked and owned by the robot
  robot.set_state(Object_State_STATE_PLACING);
  sendScene();

  if (!removeObject(object))
  {
    ROS_ERROR_STREAM("[DummyRobotArmController] Dropping failed, i.e., unable to delete object.)");
    // TODO update the state in the scene
    return false;
  }

  // "place" for 1.5 sec
  ros::Rate(ros::Duration(1.5)).sleep();

  // the robot is idle, the object is gone
  robot.set_state(Object_State_STATE_IDLE);
  sendScene();

  return true;
}

bool DummyRobotArmController::pickAndPlace(Object& robot, Object& object, Object& location, bool simulateOnly)
{
  auto robot_id = robot.id();
  auto object_id = object.id();
  auto target = location;

  ROS_INFO_STREAM("[DummyRobotArmController] \"Picking and placing\" for 3 seconds...");

  // the robot is picking, the object is picked and owned by the robot
  resolveObject(robot_id)->set_state(Object_State_STATE_PICKING);
  resolveObject(object_id)->set_state(Object_State_STATE_PICKED);
  resolveObject(object_id)->set_owner(robot_id);
  sendScene();

  // "pick" for 1.5 sec
  ros::Rate(ros::Duration(1.5)).sleep();

  // the robot is placing, the object is still picked and owned by the robot
  resolveObject(robot_id)->set_state(Object_State_STATE_PLACING);
  sendScene();

  if (target.type() == Object_Type_COLLABORATION_ZONE && target.owner() != robot_id)
  {
    // if we want to place at a collaboration zone we must own it
    ROS_ERROR_STREAM("[DummyRobotArmController] Unable to place to collab zone "
                     << target.id() << " which is owned by " << target.owner() << " and we are " << robot_id);
    // TODO update the state in the scene
    return false;
  }

  // "place" for 1.5 sec
  ros::Rate(ros::Duration(1.5)).sleep();

  resolveObject(object_id)->mutable_pos()->set_x(target.pos().x());
  resolveObject(object_id)->mutable_pos()->set_y(target.pos().y());
  resolveObject(object_id)->mutable_pos()->set_z(target.pos().z() - target.size().height() / 2 +
                                                 resolveObject(object_id)->size().height() / 2);
  resolveObject(object_id)->mutable_orientation()->set_z(target.orientation().z());
  resolveObject(object_id)->mutable_orientation()->set_w(target.orientation().w());
  // the robot is idle, the object is gone
  // the robot is idle, the object is stationary and has no owner
  resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
  resolveObject(object_id)->set_state(Object_State_STATE_STATIONARY);
  resolveObject(object_id)->set_owner("");
  sendScene();

  return true;
}

bool DummyRobotArmController::pick(const std::string& robot_name, const std::string& object_name, bool simulateOnly)
{
  // TODO
  return false;
}
bool DummyRobotArmController::place(const std::string& robot_name, const std::string& location_name, bool simulateOnly)
{
  // TODO
  return false;
}
bool DummyRobotArmController::drop(const std::string& robot_name, const std::string& bin_name, bool simulateOnly)
{
  // TODO
  return false;
}

bool DummyRobotArmController::moveToPose(Object& robot, geometry_msgs::Pose pose, bool simulateOnly)
{
  auto robot_id = robot.id();

  ROS_INFO_STREAM("[DummyRobotArmController] \"Moving\" for 2 seconds...");
  // the robot is moving
  resolveObject(robot_id)->set_state(Object_State_STATE_MOVING);
  sendScene();

  // "move" for 2 sec
  ros::Rate(ros::Duration(2)).sleep();

  // the robot is idle again
  resolveObject(robot_id)->set_state(Object_State_STATE_IDLE);
  sendScene();

  return true;
}

bool DummyRobotArmController::configureCollaborationZone(Object& zone, const std::string& owner)
{
  if (zone.owner() == owner)
  {
    ROS_WARN_STREAM(
        "[DummyRobotArmController] configureCollaborationZone was called, but it already had the desired owner "
        << owner);
    return false;
  }

  zone.set_owner(owner);
  if (zone.owner() != robot_name_)
  {
    // the zone is blocked, because we do NOT own it
    ROS_INFO_STREAM("[DummyRobotArmController] Blocking collaboration zone " << zone.id());
  }
  else
  {
    // we own it
    ROS_INFO_STREAM("[DummyRobotArmController] Freeing collaboration zone " << zone.id());
  }

  return true;
}

bool DummyRobotArmController::reachableLocation(const Object& robot, const Object& location, const Object& object)
{
  // we pretend the location is an object and try to reach it
  return reachableObject(robot, location);
}

bool DummyRobotArmController::reachableObject(const Object& robot, const Object& object)
{
  double dx = object.pos().x() - robot.pos().x();
  double dy = object.pos().y() - robot.pos().y();
  double dz = object.pos().z() - robot.pos().z();

  // if the location is within a "cone" with a radius of 50mm around the robot base, it is too close to place
  if (std::sqrt(dx * dx + dy * dy) < 0.05)
  {
    return false;
  }

  // if the location is more than 750mm away from the robot base it is too far away
  if (std::sqrt(dx * dx + dy * dy + dz * dz) > 0.8)
  {
    return false;
  }

  // otherwise, we can reach it
  return true;
}

DummyRobotArmController::DummyRobotArmController(const ros::NodeHandle& nodeHandle, const std::string& cellName,
                                                 const std::string& robotName)
  : Controller(nodeHandle), RobotArmController(nodeHandle, cellName, robotName)
{
}
