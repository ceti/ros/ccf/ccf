//
// Created by Sebastian Ebert on 29.10.21.
//
#include "ccf/connection/RosConnection.h"
#include <ros/ros.h>
#include <memory>
#include <std_msgs/String.h>

RosConnection::RosConnection(const ros::NodeHandle& n, int input_queue_size, int output_queue_size)
  : node_handle{ n }, input_queue_size{ input_queue_size }, output_queue_size{ output_queue_size }
{
}

void RosConnection::setHandle(const ros::NodeHandle& n)
{
  node_handle = n;
}

bool RosConnection::listen(const std::string& channel)
{
  ros_topics.push_back(channel);
  return true;
}

bool RosConnection::initializeConnection(std::function<void(std::string, std::string)> callback)
{
  messageReceivedCallback = callback;
  ros_spinner_thread = std::make_unique<std::thread>(&RosConnection::receiveMessages, this);
  return true;
}

bool RosConnection::addPublisher(const std::string& channel)
{
  if (ros_publishers.count(channel) == 0)
  {
    ros::Publisher pub = node_handle.advertise<std_msgs::String>(channel, output_queue_size);
    ros_publishers.insert(std::pair<std::string, ros::Publisher>(channel, pub));
    return true;
  }
  return false;
}

bool RosConnection::send(const std::string& channel, const std::string& message)
{
  if (!channel.empty())
  {
    for (const auto& [key, value] : ros_publishers)
    {
      if (key == channel)
      {
        std_msgs::String msg;
        msg.data = message;
        value.publish(msg);
        return true;
      }
    }
  }
  return false;
}

void RosConnection::receiveMessages()
{
  for (std::string& topic : ros_topics)
  {
    ros::Subscriber s =
        node_handle.subscribe<std_msgs::String>(topic, input_queue_size, [&](const std_msgs::StringConstPtr& msg) {
          messageReceivedCallback(topic, msg->data);
        });
    ros_subscribers.push_back(s);
  }
}

RosConnection::~RosConnection()
{
  ros_spinner_thread->join();
}
