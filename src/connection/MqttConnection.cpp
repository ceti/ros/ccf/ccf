//
// Created by sebastian on 04.05.21.
//

#include "ccf/connection/MqttConnection.h"
#include <ros/ros.h>
#include <thread>

bool MqttConnection::send(const std::string& channel, const std::string& message)
{
  try
  {
    int length = message.size();
    void* data = (void*)message.c_str();

    auto pub_msg = mqtt::make_message(channel, data, length);
    pub_msg->set_qos(QOS);
    pub_msg->set_retained(false);
    auto rsp = cli_.publish(pub_msg);
    rsp->wait_for(100);
    ROS_INFO_STREAM("[MqttConnection] Published to topic "
                    << channel << " with length " << message.size() << "."
                    << " with result " << MQTTReasonCode_toString((MQTTReasonCodes)rsp->get_reason_code()));
  }
  catch (const mqtt::exception& exc)
  {
    ROS_ERROR_STREAM("[MqttConnection] Unable to publish to topic " << channel << " because " << exc.what());
    return false;
  }
  return true;
}

bool MqttConnection::initializeConnection(std::function<void(std::string, std::string)> callback)
{
  ROS_INFO_STREAM("[MqttConnection] Trying to connect client " << cli_.get_client_id() << " to MQTT server.");

  // establish the connection
  try
  {
    try
    {
      auto rsp = cli_.connect();
      rsp->wait();
    }
    catch (const mqtt::exception& exception)
    {
      ROS_ERROR_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Unable to establish connection. Exception: "
                                            << exception.to_string());
    }

    if (!cli_.is_connected())
    {
      ROS_ERROR_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Not connected.");
      return false;
    }

    ROS_INFO_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Subscribing to " << topics.size() << " topics.");
    for (const auto& topic : topics)
    {
      auto rsp = cli_.subscribe(topic, QOS);
      rsp->wait();
      ROS_INFO_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Subscribing to topic " << topic
                                           << " with result "
                                           << MQTTReasonCode_toString((MQTTReasonCodes)rsp->get_reason_code()));
    }
  }
  catch (const mqtt::exception& e)
  {
    ROS_ERROR_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Unable to connect to " << cli_.get_server_uri()
                                          << ". Error '" << e.get_error_str() << "'");
    return false;
  }

  messageReceivedCallback = callback;

  cli_.set_message_callback([&](auto msg) {
    if (!msg->is_duplicate() && !msg->is_retained())
    {
      ROS_INFO_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] processing message on topic "
                                           << msg->get_topic() << " in separate thread.");
      std::thread(messageReceivedCallback, msg->get_topic(), msg->get_payload_str()).detach();
    }
    else
    {
      ROS_WARN_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Skipping duplicate/retained message on topic "
                                           << msg->get_topic() << ".");
    }
  });

  return true;
}

MqttConnection::~MqttConnection()
{
  cli_.disconnect();
}

bool MqttConnection::listen(const std::string& topic)
{
  if (cli_.is_connected())
  {
    auto rsp = cli_.subscribe(topic, QOS);
    rsp->wait();
    ROS_INFO_STREAM("[MqttConnection] [" << cli_.get_client_id() << "] Subscribing to topic " << topic
                                         << " with result "
                                         << MQTTReasonCode_toString((MQTTReasonCodes)rsp->get_reason_code()));
  }
  topics.push_back(topic);
  return true;
}

MqttConnection::MqttConnection(const std::string& mqttAddress, const std::string& client_id)
  : cli_(mqttAddress, client_id), topics()
{
  connOpts_.set_keep_alive_interval(20);
  connOpts_.set_connect_timeout(1);
  connOpts_.set_clean_session(false);
}